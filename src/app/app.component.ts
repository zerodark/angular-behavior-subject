import {Component, OnDestroy} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {PersonaService} from "./services/persona.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-example';

  public mensaje: string = 'persona1';
  public mensaje2: string = 'persona2'
  public generico = new BehaviorSubject<any>('test');
  contador = 1;

  constructor(private personaService: PersonaService) {

    this.personaService.getDataFromServer().subscribe(result => {
      console.log('from subscription...');
      console.log(result);
      this.mensaje = this.mensaje + ' - ' + JSON.stringify(result);
    });

    // persona 1
    this.generico.subscribe(data => {
      console.log('recibiendo #1...:' + data);
      this.mensaje = this.mensaje + ' - ' + data;
      // envie  un mail

    });

    // persona 2
    this.generico.subscribe(data => {
      console.log('recibiendo #2...:' + data);
      this.mensaje2 = '' + this.contador++;
      // guarde en base de datos
      // actualice grafica
    });

  }

  public enviarEvento(texto: string): void {
    this.generico.next(texto);
  }


}
