import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Injectable} from "@angular/core";


@Injectable({
  providedIn: 'root',
})
export class PersonaService {

  constructor(
    private http: HttpClient,
  ) {
  }

  public getDataFromServer(): Observable<any> {

    return new Observable((observer) => {

      let url = 'http://localhost:8586/persona/notification/sse'

      const eventSource = new EventSource(url);

      eventSource.addEventListener('persona-result', (message: any) => {
        observer.next(message.data);
      });

      return () => eventSource.close();
    });
  }

}
